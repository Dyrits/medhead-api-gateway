package com.medhead.gateway.utility;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.experimental.UtilityClass;

@UtilityClass
public class ObjectMapperUtility {

    private final ObjectMapper mapper = new ObjectMapper();

    public <T> T map(Object object, Class<T> type){
        return mapper.convertValue(object, type);
    }

}