package com.medhead.gateway;

import com.medhead.gateway.redis.RedisHashComponent;
import com.medhead.gateway.authorization.Services;
import com.medhead.gateway.model.Key;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
@Slf4j
public class MedHeadApiGatewayApplication {

    @Autowired
    private RedisHashComponent redisHashComponent;
    private final Environment environment;

    public MedHeadApiGatewayApplication(Environment environment) { this.environment = environment; }

    public static void main(String[] args) {
        log.info("MedHeadApiGatewayApplication started");
        SpringApplication.run(MedHeadApiGatewayApplication.class, args);
    }

    @Bean
    public RouteLocator build(RouteLocatorBuilder builder) {
        return builder.routes()
                .route(Services.EMERGENCY_API, route -> route.path("/api/emergency/**")
                        .filters(path -> path.stripPrefix(2)).uri(environment.getProperty("spring.api.emergency.url")))
                .route(Services.EXAMPLE_API, route -> route.path("/api/example/**")
                        .filters(path -> path.stripPrefix(2)).uri(environment.getProperty("spring.api.example.url")))
                /*
                Additional routes can be added here, for example:
                .route(Services.[SERVICE_NAME], route -> route.path("/api/[SERVICE_ROUTE]/**")
                .filters(path -> path.stripPrefix(2)).uri("http://localhost:[SERVICE_PORT]"))
                 */
                .build();
    }

    @PostConstruct
    public void initialize() {
        List<Key> keys = new ArrayList<>();
        keys.add(new Key("74C89815836151F2CC4A030DC40C2FD1", List.of(Services.EMERGENCY_API)));
        keys.add(new Key("BC12CDF75453223E88237E529E5D53F5", List.of(Services.EXAMPLE_API)));
        /*
         Additional API keys can be added here, for example:
         keys.add(new Key("[API_KEY]", List.of(Services.[SERVICE_NAME])));

         Keys are hardcoded for the moment, but authorized users will be able to generate new keys with the dedicated API.
         */

        if (redisHashComponent.hashValues(Services.RECORD_KEY).isEmpty()) {
            keys.forEach(key -> redisHashComponent.hashSet(Services.RECORD_KEY, key.getKey(), key));
        }
    }

}
