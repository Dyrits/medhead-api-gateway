package com.medhead.gateway.authorization;

import com.medhead.gateway.redis.RedisHashComponent;
import com.medhead.gateway.model.Key;
import com.medhead.gateway.utility.ObjectMapperUtility;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.cloud.gateway.route.Route;
import org.springframework.cloud.gateway.support.ServerWebExchangeUtils;
import org.springframework.core.Ordered;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.List;

@Component
@Slf4j
public class AuthorizationFilter implements GlobalFilter, Ordered {

    @Autowired
    private RedisHashComponent redisHashComponent;

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        List<String> authorization = exchange.getRequest().getHeaders().get("API-Authorization");
        log.info("API-Authorization: {}", authorization);
        Route route = exchange.getAttribute(ServerWebExchangeUtils.GATEWAY_ROUTE_ATTR);
        String service = route == null ? null : route.getId();
        if (service == null || CollectionUtils.isEmpty(authorization) || !isAuthorized(service, authorization.get(0))) {
            throw new ResponseStatusException(
                    HttpStatus.UNAUTHORIZED,
                    "You need to set a correct value to API-Authorization in your request header in order to use this API."
            );
        }
        return chain.filter(exchange);
    }

    private boolean isAuthorized(String service, String authorization) {
        Object object = redisHashComponent.hashGet(Services.RECORD_KEY, authorization);
        return object != null && ObjectMapperUtility.map(object, Key.class).getServices().contains(service);
    }

    @Override
    public int getOrder() {
        return Ordered.LOWEST_PRECEDENCE;
    }
}
