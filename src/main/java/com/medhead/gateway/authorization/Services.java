package com.medhead.gateway.authorization;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Services {
    public static final String EMERGENCY_API = "3460CF4C71DE2AC113D3A81CE433601A";
    public static final String EXAMPLE_API = "354EC1B77C854AA0AC64BA70FAA78192";
    /*
    Services can be added here, with their corresponding identifies, for example:
    public static final String [SERVICE_NAME] = "[SERVICE_ID]"; // MD5
    */
    public static final String RECORD_KEY = "API_KEYS";
}
