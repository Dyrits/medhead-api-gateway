package com.medhead.gateway.model;

import lombok.*;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
public class Key {
    private String key;
    private List<String> services;
}
