package com.medhead.gateway.redis;

import com.medhead.gateway.utility.ObjectMapperUtility;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;

@Component
@Slf4j
public class RedisHashComponent {

    private final RedisTemplate<String, Object> redisTemplate;

    public RedisHashComponent(RedisTemplate<String, Object> redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    public void hashSet(String key, Object hashKey, @NotNull Object value) {
        redisTemplate.opsForHash().put(key, hashKey, ObjectMapperUtility.map(value, Map.class));
    }

    public List<Object> hashValues(String key) {
        return redisTemplate.opsForHash().values(key);
    }

    public Object hashGet(String key, Object hashKey) {
        return redisTemplate.opsForHash().get(key, hashKey);
    }


}
