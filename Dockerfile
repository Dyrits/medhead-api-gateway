FROM eclipse-temurin:11-alpine
RUN apk update && apk upgrade && apk add --no-cache bash
ENV HOME=/app
RUN mkdir -p $HOME
WORKDIR $HOME